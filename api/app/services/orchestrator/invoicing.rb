module Orchestrator
  class Invoicing < Service
    def initialize(item, property_id, service_code, user_token)
      super(property_id, service_code, user_token)
      @item = item
    end

    def create_invoice
      path = "#{orchestrator_url}/invoices"
      ob = { invoice: @item }
      Network::RequestsHelper.post(path, ob, headers)
    end
  end
end
