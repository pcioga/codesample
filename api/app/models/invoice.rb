class Invoice < ApplicationRecord

  def self.create_and_submit(invoice, user_token)
    invoice = handle_invoice_items(invoice)
    customer_id = invoice[:invoice_customer_attributes][:customer_id]
    predraft = Invoice.create(invoice)
    predraft.notes = add_invoice_meta_info(invoice)
    submit_invoice(predraft, customer_id, user_token)
  end

  def self.submit_invoice(predraft, customer_id, user_token)
    res = req_submit_invoice(predraft, user_token)

    if res[:status] == 'error'
      predraft.update(error_message: res[:error])
      return predraft
    end

    predraft.update(Orchestrator::Serializers::Invoice.from(res[:invoice]))

    predraft
  end

  def self.req_submit_invoice(invoice, user_token)
    Orchestrator::Invoicing.new(
      InvoiceSerializer.new(invoice).as_json,
      invoice.folio.property_id,
      invoice.service_code,
      user_token
    ).create_invoice
  end

  def self.handle_invoice_items(invoice_params)
    {
      **invoice_params,
      invoice_items_attributes: invoice_params[:invoice_items_attributes].map do |invoice_item|
        invoice_item_attributes(invoice_item)
      end
    }
  end
end
