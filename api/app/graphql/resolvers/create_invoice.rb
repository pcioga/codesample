class Resolvers::CreateInvoice < GraphQL::Function
  argument :invoice, !NewInvoiceInputObjectType

  type InvoiceType

  def call(_obj, args, ctx)
    invoice = args[:invoice].to_h.deep_symbolize_keys
    invoice[:status] = 'pre-draft'

    Invoice.create_and_submit(invoice.except(:final_customer), ctx[:user_token])
  rescue ActiveRecord::RecordInvalid => e
    GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
  end

end
