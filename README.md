### HotelCracy code sample

I decided to share the code of a simple user flow of the project I’ve been working on for the last couple of years. As you may recall, I told you I’ve been working on a RedLight spin-off called HotelCracy. To put it simple, HotelCracy is a SaaS integrator for different Hotel services. On the following example I’ll guide you through the backend code for the creation of an Invoice, on an SaaS called InvoiceXpress. 

I’ll have to explain you a little bit of the project’s architecture for your better understanding. We have a frontend app, developed on React, which sends all requests to a Ruby on Rails Graphql API that we should further on refer to simply as API. After our API receives the frontend request it redirects the request to a different server that we called Orchestrator. The Orchestrator deals with whatever operation the API requires. It may be read/write operations on the DB, or connecting to third party services, which is what we want to show on this example. So if we want to create an invoice, our Orchestrator will connect to our Integration Broker server, which is responsible for translating HotelCracy requests to the required SaaS request, and the SaaS response into a unified Hotelcracy response. Still with me?

For this sample purpose I've deleted all the irrelevant code and files.  


### Code walkthrough

So on our API server we have a `create_invoice` graphql resolver which is called by the graphql controller, when a frontend request to create an invoice is triggered. Our resolver gathers the data sent from the frontend on a InvoiceInputType graphql object.

[create_invoice.rb](https://gitlab.com/pcioga/codesample/-/blob/master/api/app/graphql/resolvers/create_invoice.rb)

We send the data to `create_and_submit` function from our Invoice model. We followed a fat model vs skinny controller approach.
Firstly we serialize the invoice data then we create a first version of the invoice, that we call predraft, and finally we call the `submit_invoice` function.


[invoice.rb](https://gitlab.com/pcioga/codesample/-/blob/master/api/app/models/invoice.rb)

The `submit_invoice` function will make the request to the Orchestrator and later on will deal with the Orchestratator response. So the `req_submit_invoice` function is called and we send a user token, for authentication between serverrs, and the invoice data to the Orchestratator through the Orchestrator invoicing service.

[invoicing.rb](https://gitlab.com/pcioga/codesample/-/blob/master/api/app/services/orchestrator/invoicing.rb)

We initialize this Invoicing class, by sending the invoice data (item), property_id (Id that identifies the hotel), service code (SaaS code identifier) and the above mentioned, user token. We then define the Orchestator path and start a post request with the invoice data.


On the Orchestrator side we defined similar routes for different types of items related to invoices as credit notes even tho they are not important to our sample, it demonstrates how the Orchestrator routes different types of requests.

[routes.rb](https://gitlab.com/pcioga/codesample/-/blob/master/orchestrator/config/routes.rb)

So we are directed to the `invoices_controller` and call up the `create_finalized` action. On the Orchestrator we deal with two types of module, the Interactors, responsible by the reponse needed to interact with the Integration Broker, and the Connectors, responsible by connecting to the Integration Broker.

On the Invoices controller, we begin by instantiating a `interactator` variable by initializing a InvoicingIteractor object, while validating the user subscription of the SaaS and then calling the `create_finalized` method on the interactator to proceed with the invoice creation. 

[invoices_controller.rb](https://gitlab.com/pcioga/codesample/-/blob/master/orchestrator/app/controllers/invoices_controller.rb)

The invoice interactator will then initialize the respcetive connector so that we can connect to the Integration Broker.

[invoicing_interactor.rb](https://gitlab.com/pcioga/codesample/-/blob/master/orchestrator/models/interactors/invoicing_interactor.rb)

The connecto simply defines the path and the post request to the Integration Broker server.

[invoicing_connector.rb](https://gitlab.com/pcioga/codesample/-/blob/master/orchestrator/models/connectors/invoicing_connector.rb)


On the Integration Broker side, we have an identical route configuration, so the Orchestrator will now be received by the `created_finalize` action on the Invoices Controller. I haven't included any piece of code of the Integration Broker side since I wasn't the main author, even tho had contributted.
After the SaaS responds to our request, we cover the reverse path by sendig the response to the Orchestrator and then back to the API server.

So now we are back almost where we started, at the Invoice model on the API and we just validate the retrieved data and update our predraft with a serialized response and that's it!


Hope everything was clear, if you have any question you would like to run by me, just hit me at pcioga@redlightsoft.com
Thank you


