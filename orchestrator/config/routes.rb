Rails.application.routes.draw do
  
  %w[invoices credit_notes debit_notes].each do |type|
    post type, to: "#{type}#create_finalized"
  end

end
