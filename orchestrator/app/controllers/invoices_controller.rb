class InvoicesController < ApplicationController

  INVOICES_TYPE = "invoices".freeze

  def create_finalized
    result = interactor.create_finalized(body_data)
    result.keys.include?(:error) ? respond(result) : respond(result, :created)
  end

  private

  def interactor
    @interactor ||= Interactors::InvoicingInteractor.new(subscription_headers,
                                                         @user_token,
                                                         INVOICES_TYPE)
  end
end
