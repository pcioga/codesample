module Interactors
  class InvoicingInteractor
    attr_reader :subscription, :entity_type, :authorization_hc

    def initialize(subscription_headers, authorization_hc, entity_type)
      @subscription = Helpers::AuthHelper.verify_subscription_auth(
        subscription_headers,
        authorization_hc
      )
      @entity_type = entity_type
      @authorization_hc = authorization_hc
    end

    def create_finalized(body_data)
      connector.create_finalized(
        subscription,
        authorization_hc,
        entity_type
      )
    end

    private

    def connector
      @connector ||= Connectors::InvoicingConnector
    end
  end
end
