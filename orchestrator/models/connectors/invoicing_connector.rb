module Connectors
  class InvoicingConnector < IntBrokerConnector
    CLIENTS_TYPE = "customers".freeze
    TAXES_TYPE = "taxes".freeze

    def self.create_finalized(body_data, subscription, authorization_hc, documents_type)
      path = "#{int_broker_url}/#{documents_type}"
      post_to(path, body_data, req_headers(subscription, authorization_hc))
    rescue Timeout::Error
      time_out_error(MODULE_NAME)
    end

  end
end
